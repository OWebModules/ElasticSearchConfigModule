﻿using ElasticSearchConfigModule.Factories;
using ElasticSearchConfigModule.Models;
using ElasticSearchConfigModule.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchConfigModule
{
    public class ElasticSearchConfigController : AppController
    {
        private clsLocalConfig localConfig;

        public async Task<ResultItem<ElasticSearchConfig>> GetConfig(string idRefItem, clsOntologyItem relationTypeIndex, clsOntologyItem relationTypeType, clsOntologyItem direction)
        {
            var resultOItem = await GetOItem(idRefItem);
            var resultFactory = new ResultItem<ElasticSearchConfig>
            {
                ResultState = localConfig.Globals.LState_Success.Clone()
            };

            if (resultOItem.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                resultFactory.ResultState = localConfig.Globals.LState_Error.Clone();
                return resultFactory;
            }

            resultFactory = await GetConfig(resultOItem.Result.ObjectItem, relationTypeIndex, relationTypeType, direction);
            return resultFactory;
        }

        public async Task<ResultOItem<ClassObject>> GetOItem(string idObject)
        {
            var serviceElastic = new ServiceAgentElastic(localConfig);
            var result = await serviceElastic.GetOItem(idObject);
            return result;
        }

        public async Task<ResultItem<ElasticSearchConfig>> GetConfig(clsOntologyItem refItem, clsOntologyItem relationTypeIndex, clsOntologyItem relationTypeType, clsOntologyItem direction)
        {
            var elasticAgent = new ServiceAgentElastic(localConfig);

            var result = new ResultItem<ElasticSearchConfig>
            {
                ResultState = localConfig.Globals.LState_Success.Clone()
            };

            var resultService = await elasticAgent.GetIndex(refItem, relationTypeIndex, relationTypeType, direction);

            if (resultService.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }


            var factory = new IndexFactory();

            var resultFactory = await factory.CreateIndexItem(resultService.Result, localConfig);

            if (resultFactory.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }

            return resultFactory;
        }

        public async Task<ResultItem<ElasticSearchConfig>> GetConfig(string idIndex, string idType)
        {
            var elasticAgent = new ServiceAgentElastic(localConfig);

            var result = new ResultItem<ElasticSearchConfig>
            {
                ResultState = localConfig.Globals.LState_Success.Clone()
            };

            var resultIndex = await GetOItem(idIndex);

            result.ResultState = resultIndex.ResultState;
            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            var resultType = await GetOItem(idType);

            result.ResultState = resultType.ResultState;
            if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            var resultService = await elasticAgent.GetIndex(resultIndex.Result.ObjectItem, resultType.Result.ObjectItem);

            if (resultService.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }


            var factory = new IndexFactory();

            var resultFactory = await factory.CreateIndexItem(resultService.Result, localConfig);

            if (resultFactory.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }

            return resultFactory;
        }

        public ElasticSearchConfigController(Globals globals) : base(globals)
        {
            localConfig = new clsLocalConfig(globals);
        }
    }

}
