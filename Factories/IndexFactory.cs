﻿using ElasticSearchConfigModule.Models;
using ElasticSearchConfigModule.Services;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchConfigModule.Factories
{
    public class IndexFactory
    {
        public async Task<ResultItem<ElasticSearchConfig>> CreateIndexItem(IndexRaw indexRaw, clsLocalConfig localConfig)
        {
            var taskResult = await Task.Run<ResultItem<ElasticSearchConfig>>(() =>
            {
                var result = new ResultItem<ElasticSearchConfig>
                {
                    ResultState = localConfig.Globals.LState_Success.Clone()
                };

                var indexes = new List<ElasticSearchConfig>();

                if (indexRaw.Direction != null)
                {
                    if (indexRaw.Direction.GUID == localConfig.Globals.Direction_LeftRight.GUID)
                    {
                        indexes = (from indexAndRef in indexRaw.IndexAndRef
                                   join refAndType in indexRaw.RefAndType on indexAndRef.ID_Object equals refAndType.ID_Object
                                   join serverPort in indexRaw.IndexToServerPort on indexAndRef.ID_Other equals serverPort.ID_Object
                                   join serverPortToServer in indexRaw.ServerPortToServer on serverPort.ID_Other equals serverPortToServer.ID_Object
                                   join serverPortToPort in indexRaw.ServerPortToPort on serverPort.ID_Other equals serverPortToPort.ID_Object
                                   select new ElasticSearchConfig
                                   {
                                       IdDirection = indexRaw.Direction.GUID,
                                       IdRelationTypeIndex = indexRaw.RelationTypeIndex.GUID,
                                       NameRelationTypeIndex = indexRaw.RelationTypeIndex.Name,
                                       IdRelationTypeType = indexRaw.RelationTypeType.GUID,
                                       NameRelationTypeType = indexRaw.RelationTypeType.Name,
                                       IdIndex = indexAndRef.ID_Other,
                                       NameIndex = indexAndRef.Name_Other,
                                       IdRefItem = indexAndRef.ID_Object,
                                       NameRefItem = indexAndRef.Name_Object,
                                       IdServerPort = serverPort.ID_Other,
                                       NameServerPort = serverPort.Name_Other,
                                       IdType = refAndType.ID_Other,
                                       NameType = refAndType.Name_Other,
                                       IdServer = serverPortToServer.ID_Other,
                                       NameServer = serverPortToServer.Name_Other,
                                       IdPort = serverPortToPort.Name_Other,
                                       NamePort = serverPortToPort.Name_Other,
                                       Port = Convert.ToInt32(serverPortToPort.Name_Other)
                                   }).ToList();


                    }
                    else
                    {
                        indexes = (from indexAndRef in indexRaw.IndexAndRef
                                   join refAndType in indexRaw.RefAndType on indexAndRef.ID_Object equals refAndType.ID_Object
                                   join serverPort in indexRaw.IndexToServerPort on refAndType.ID_Object equals serverPort.ID_Object
                                   join serverPortToServer in indexRaw.ServerPortToServer on serverPort.ID_Other equals serverPortToServer.ID_Object
                                   join serverPortToPort in indexRaw.ServerPortToPort on serverPort.ID_Other equals serverPortToPort.ID_Object
                                   select new ElasticSearchConfig
                                   {
                                       IdDirection = indexRaw.Direction.GUID,
                                       IdRelationTypeIndex = indexRaw.RelationTypeIndex.GUID,
                                       NameRelationTypeIndex = indexRaw.RelationTypeIndex.Name,
                                       IdRelationTypeType = indexRaw.RelationTypeType.GUID,
                                       NameRelationTypeType = indexRaw.RelationTypeType.Name,
                                       IdIndex = indexAndRef.ID_Other,
                                       NameIndex = indexAndRef.Name_Other,
                                       IdRefItem = indexAndRef.ID_Object,
                                       NameRefItem = indexAndRef.Name_Object,
                                       IdServerPort = serverPort.ID_Other,
                                       NameServerPort = serverPort.Name_Other,
                                       IdType = refAndType.ID_Other,
                                       NameType = refAndType.Name_Other,
                                       IdServer = serverPortToServer.ID_Other,
                                       NameServer = serverPortToServer.Name_Other,
                                       IdPort = serverPortToPort.Name_Other,
                                       NamePort = serverPortToPort.Name_Other,
                                       Port = Convert.ToInt32(serverPortToPort.Name_Other)
                                   }).ToList();
                    }
                }
                else
                {
                    indexes = (from serverPort in indexRaw.IndexToServerPort.Where(ixToSP => ixToSP.ID_Object == indexRaw.IndexItem.GUID)
                               join serverPortToServer in indexRaw.ServerPortToServer on serverPort.ID_Other equals serverPortToServer.ID_Object
                               join serverPortToPort in indexRaw.ServerPortToPort on serverPort.ID_Other equals serverPortToPort.ID_Object
                               select new ElasticSearchConfig
                               {
                                   IdIndex = indexRaw.IndexItem.GUID,
                                   NameIndex = indexRaw.IndexItem.Name,
                                   IdServerPort = serverPort.ID_Other,
                                   NameServerPort = serverPort.Name_Other,
                                   IdType = indexRaw.TypeItem.GUID,
                                   NameType = indexRaw.TypeItem.Name,
                                   IdServer = serverPortToServer.ID_Other,
                                   NameServer = serverPortToServer.Name_Other,
                                   IdPort = serverPortToPort.Name_Other,
                                   NamePort = serverPortToPort.Name_Other,
                                   Port = Convert.ToInt32(serverPortToPort.Name_Other)
                               }).ToList();
                }
                

                result.Result = indexes.FirstOrDefault();

                return result;
            });

            return taskResult;
        }
    }
}
