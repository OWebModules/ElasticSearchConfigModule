﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchConfigModule.Models
{
    public class ElasticSearchConfig
    {
        public string IdIndex { get; set; }
        public string NameIndex { get; set; }

        public string IdRefItem { get; set; }
        public string NameRefItem { get; set; }

        public string IdRelationTypeIndex { get; set; }
        public string NameRelationTypeIndex { get; set; }

        public string IdRelationTypeType { get; set; }
        public string NameRelationTypeType { get; set; }

        public string IdDirection { get; set; }

        public string IdType { get; set; }
        public string NameType { get; set; }

        public string IdServerPort { get; set; }
        public string NameServerPort { get; set; }

        public string IdServer { get; set; }
        public string NameServer { get; set; }

        public string IdPort { get; set; }
        public string NamePort { get; set; }
        public int Port { get; set; }
    }
}
