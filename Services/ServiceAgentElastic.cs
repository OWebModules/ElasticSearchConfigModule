﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchConfigModule.Services
{
    public class ServiceAgentElastic : ElasticBaseAgent
    {
        private clsLocalConfig localConfig;

        public async Task<ResultOItem<ClassObject>> GetOItem(string idObject)
        {
            var taskResult = await Task.Run<ResultOItem<ClassObject>>(() =>
            {
                var result = new ResultOItem<ClassObject>()
                {
                    ResultState = localConfig.Globals.LState_Success.Clone(),
                    Result = new ClassObject()
                };

                var dbReader = new OntologyModDBConnector(localConfig.Globals);

                var objectItem = dbReader.GetOItem(idObject, localConfig.Globals.Type_Object);

                if (objectItem.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    result.ResultState = localConfig.Globals.LState_Error.Clone();
                    return result;
                }

                result.Result.ObjectItem = objectItem;

                var classItem = dbReader.GetOItem(objectItem.GUID_Parent, localConfig.Globals.Type_Class);

                if (classItem.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    result.ResultState = localConfig.Globals.LState_Error.Clone();
                    return result;
                }

                result.Result.ClassItem = classItem;

                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<IndexRaw>> GetIndex(clsOntologyItem refItem, clsOntologyItem relationTypeIndex, clsOntologyItem relationTypeType, clsOntologyItem directionItem)
        {
            var taskResult = await Task.Run<ResultItem<IndexRaw>>(() =>
            {
                var result = new ResultItem<IndexRaw>
                {
                    ResultState = localConfig.Globals.LState_Success.Clone(),
                    Result = new IndexRaw
                    {
                        Direction = directionItem,
                        RelationTypeIndex = relationTypeIndex,
                        RelationTypeType = relationTypeType
                    }
                };

                var searchRefAndIndex = new List<clsObjectRel>();
                if (directionItem.GUID == localConfig.Globals.Direction_LeftRight.GUID)
                {
                    searchRefAndIndex = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = refItem.GUID,
                            ID_RelationType = relationTypeIndex.GUID,
                            ID_Parent_Other = localConfig.OItem_class_indexes__elastic_search_.GUID
                        }
                    };
                }
                else
                {
                    searchRefAndIndex = new List<clsObjectRel> {
                        new clsObjectRel
                        {
                            ID_Other = refItem.GUID,
                            ID_RelationType = relationTypeIndex.GUID,
                            ID_Parent_Object = localConfig.OItem_class_indexes__elastic_search_.GUID
                        }
                    };
                }

                var dbReaderRefAndIndex = new OntologyModDBConnector(localConfig.Globals);
                result.ResultState = dbReaderRefAndIndex.GetDataObjectRel(searchRefAndIndex);

                if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.IndexAndRef = dbReaderRefAndIndex.ObjectRels;

                var searchIndexToServerPort = dbReaderRefAndIndex.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = directionItem.GUID == localConfig.Globals.Direction_LeftRight.GUID ? rel.ID_Other : rel.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_belongs_to.GUID,
                    ID_Parent_Other = localConfig.OItem_class_server_port.GUID
                }).ToList();

                var dbReaderIndexToServerPort = new OntologyModDBConnector(localConfig.Globals);

                if (searchIndexToServerPort.Any())
                {
                    result.ResultState = dbReaderIndexToServerPort.GetDataObjectRel(searchIndexToServerPort);
                    if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        return result;
                    }


                }

                result.Result.IndexToServerPort = dbReaderIndexToServerPort.ObjectRels;

                var searchServerPortToServer = dbReaderIndexToServerPort.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                    ID_Parent_Other = localConfig.OItem_class_server.GUID
                }).ToList();

                var dbReaderServerPortToServer = new OntologyModDBConnector(localConfig.Globals);

                if (searchServerPortToServer.Any())
                {
                    result.ResultState = dbReaderServerPortToServer.GetDataObjectRel(searchServerPortToServer);
                    if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.ServerPortToServer = dbReaderServerPortToServer.ObjectRels;

                var searchServerPortToPort = dbReaderIndexToServerPort.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                    ID_Parent_Other = localConfig.OItem_class_port.GUID
                }).ToList();

                var dbReaderServerPortToPort = new OntologyModDBConnector(localConfig.Globals);

                if (searchServerPortToPort.Any())
                {
                    result.ResultState = dbReaderServerPortToPort.GetDataObjectRel(searchServerPortToPort);
                    if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.ServerPortToPort = dbReaderServerPortToPort.ObjectRels;

                var searchRefToType = new List<clsObjectRel>();

                if (directionItem.GUID == localConfig.Globals.Direction_LeftRight.GUID)
                {
                    searchRefToType = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = refItem.GUID,
                            ID_RelationType = relationTypeType.GUID,
                            ID_Parent_Other = localConfig.OItem_class_types__elastic_search_.GUID
                        }
                    };
                }
                else
                {
                    searchRefToType = new List<clsObjectRel> {
                        new clsObjectRel
                        {
                            ID_Other = refItem.GUID,
                            ID_RelationType = relationTypeType.GUID,
                            ID_Parent_Object = localConfig.OItem_class_types__elastic_search_.GUID
                        }
                    };
                }

                var dbReaderRefToType = new OntologyModDBConnector(localConfig.Globals);

                if (searchRefToType.Any())
                {
                    result.ResultState = dbReaderRefToType.GetDataObjectRel(searchRefToType);
                    if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.RefAndType = dbReaderRefToType.ObjectRels;

                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<IndexRaw>> GetIndex(clsOntologyItem indexItem, clsOntologyItem typeItem)
        {
            var taskResult = await Task.Run<ResultItem<IndexRaw>>(() =>
            {
                var result = new ResultItem<IndexRaw>
                {
                    ResultState = localConfig.Globals.LState_Success.Clone(),
                    Result = new IndexRaw
                    {
                        IndexItem = indexItem,
                        TypeItem = typeItem
                    }
                };

                var searchRefAndIndex = new List<clsObjectRel>();

                var searchIndexToServerPort = new List<clsObjectRel>{ new clsObjectRel
                {
                    ID_Object = indexItem.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belongs_to.GUID,
                    ID_Parent_Other = localConfig.OItem_class_server_port.GUID
                } };

                var dbReaderIndexToServerPort = new OntologyModDBConnector(localConfig.Globals);

                if (searchIndexToServerPort.Any())
                {
                    result.ResultState = dbReaderIndexToServerPort.GetDataObjectRel(searchIndexToServerPort);
                    if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        return result;
                    }


                }

                result.Result.IndexToServerPort = dbReaderIndexToServerPort.ObjectRels;

                var searchServerPortToServer = dbReaderIndexToServerPort.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                    ID_Parent_Other = localConfig.OItem_class_server.GUID
                }).ToList();

                var dbReaderServerPortToServer = new OntologyModDBConnector(localConfig.Globals);

                if (searchServerPortToServer.Any())
                {
                    result.ResultState = dbReaderServerPortToServer.GetDataObjectRel(searchServerPortToServer);
                    if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.ServerPortToServer = dbReaderServerPortToServer.ObjectRels;

                var searchServerPortToPort = dbReaderIndexToServerPort.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                    ID_Parent_Other = localConfig.OItem_class_port.GUID
                }).ToList();

                var dbReaderServerPortToPort = new OntologyModDBConnector(localConfig.Globals);

                if (searchServerPortToPort.Any())
                {
                    result.ResultState = dbReaderServerPortToPort.GetDataObjectRel(searchServerPortToPort);
                    if (result.ResultState.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.ServerPortToPort = dbReaderServerPortToPort.ObjectRels;

                return result;
            });
            return taskResult;
        }

        public ServiceAgentElastic(clsLocalConfig localConfig) : base(localConfig.Globals)
        {
            this.localConfig = localConfig;
        }
    }

    public class IndexRaw
    {
        public clsOntologyItem Direction { get; set; }
        public clsOntologyItem RelationTypeIndex { get; set; }
        public clsOntologyItem RelationTypeType { get; set; }
        public clsOntologyItem IndexItem { get; set; }
        public clsOntologyItem TypeItem { get; set; }
        public List<clsObjectRel> IndexAndRef { get; set; }
        public List<clsObjectRel> IndexToServerPort { get; set; }
        public List<clsObjectRel> ServerPortToServer { get; set; }
        public List<clsObjectRel> ServerPortToPort { get; set; }

        public List<clsObjectRel> RefAndType { get; set; }
    }
}
